#pragma once

#include "list.h"
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>


void foreach(Node *, void (*)(int));

Node *map(Node *, int (*)(int));

Node *iterate(int, int, int (*)(int));

void map_mut(Node *, int (*)(int));

int foldl(Node *, int, int (*)(int, int));

bool save(Node *head, const char *filename);

bool load(Node **head, const char *filename);

bool serialize(Node *head, const char *filename);

bool deserialize(Node **head, const char *filename);

