#pragma once

int squares(int);

int cubes(int);

int abs(int);

int powerTwo(int);

int sum(int, int);

int min(int, int);

int max(int, int);

void printIntLn(const int);

void printInt(const int);
