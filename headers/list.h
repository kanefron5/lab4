#pragma once

typedef struct Node {
    int value;
    struct Node *next;
} Node;

Node *list_create(int, Node *);

Node *list_add_front(Node *, int);

Node *list_add_back(Node *, int);

int list_length(Node *);

Node *list_node_at(Node *, int);

void list_set_node_at(Node *, int, int);

Node *list_clear(Node *);

int list_sum(Node *);

void list_println(Node *);
