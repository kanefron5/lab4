#include "../headers/list.h"
#include <stdio.h>
#include <stdlib.h>


Node *list_create(int value, Node *next) {
    Node *new_node = (Node *) malloc(sizeof(Node));
    if (new_node == NULL) {
        printf("Ошибка!\n");
        exit(0);
    }
    new_node->value = value;
    new_node->next = next;
    return new_node;
}

Node *list_add_front(Node *head, int value) {
    Node *new_node = list_create(value, head);
    head = new_node;
    return head;
}

Node *list_add_back(Node *head, int value) {
    Node *cursor = head;

    if (head == NULL) return list_create(value, head);
    while (cursor->next != NULL) cursor = cursor->next;
    cursor->next = list_create(value, NULL);
    return head;
}

int list_length(Node *head) {
    Node *cursor = head;
    int count = 0;
    while (cursor != NULL) {
        count++;
        cursor = cursor->next;
    }
    return count;
}


Node *list_node_at(Node *head, int index) {
    Node *cursor = head;

    int i = 0;
    if (list_length(head) < index || index < 0) return NULL;
    while (i < index) {
        cursor = cursor->next;
        i++;
    }
    return cursor;
}

void list_set_node_at(Node *head, int index, int value) {
    Node *cursor = head;

    int i = 0;
    if (list_length(head) < index || index < 0) return;
    while (i < index) {
        cursor = cursor->next;
        i++;
    }
    cursor->value = value;
}

Node *list_clear(Node *node) {
    Node *nodeForFree;
    while (node != NULL) {
        nodeForFree = node;
        node = node->next;
        free(nodeForFree);
    }
    return node;
}


int list_sum(Node *node) {
    int sum = 0;
    while (node != NULL) {
        sum += node->value;
        node = node->next;
    }
    return sum;
}


void list_println(Node *node) {
    int first = 1;

    if (node == NULL) {
        printf("Список пуст!");
        return;
    }

    printf("[");
    while (node != NULL) {
        if (first == 1) {
            printf("%d", node->value);
            first = 0;
        } else printf(", %d", node->value);
        node = node->next;
    }
    printf("]\n");
}



