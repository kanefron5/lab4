#include <stdio.h>
#include "../headers/list.h"
#include "../headers/listHightOrder.h"
#include "../headers/tests.h"

void println(const char *v) {
    printf("%s\n", v);
}

void test(Node *head) {
    Node *tmp;
    if (list_length(head) > 0) {
        println("3.");
        println("foreach; separate elements with spaces");
        foreach(head, printInt);
        println("");
        println("foreach; separate elements with \\n");
        foreach(head, printIntLn);

        println("\n4.");
        println("squares of the numbers from list");
        tmp = map(head, squares);
        foreach(tmp, printInt);
        println("");
        println("cubes of the numbers from list");
        tmp = map(head, cubes);
        foreach(tmp, printInt);
        println("");

        println("\n5.");
        printf("Sum: %d\n", foldl(head, 0, sum));
        printf("Min: %d\n", foldl(head, list_node_at(head, 0)->value, min));
        printf("Max: %d\n", foldl(head, list_node_at(head, 0)->value, max));

        println("\n6. Modules of the input numbers");
        map_mut(head, abs);
        list_println(head);
    }

    println("\n7. list of the powers of two");
    tmp = iterate(1, 10, powerTwo);
    list_println(tmp);

    println("8. write all elements of the list into a text file filename.txt");
    if (save(head, "filename.txt")) {
        println("true");
    } else println("false");
    println("");

    Node* array = NULL;
    println("9. read all integers from a text file filename.txt and write the saved list into tmp");
    if (load(&array, "filename.txt")) {
        println("true");
        list_println(array);
    } else println("false");
    println("");


    println("11. write all elements of the list into a binary file filename.bin");
    if (serialize(head, "filename.bin")) {
        println("true");
    } else println("false");
    println("");

    array = NULL;
    println("12. read all integers from a binary file filename.bin and write the saved list into tmp");
    if (deserialize(&array, "filename.bin")) {
        println("true");
        list_println(array);
    } else println("false");
    println("");
}

void printHelp() {
    println("Список доступных команд:");
    println("0 - добавить в начало");
    println("1 - добавить в конец");
    println("2 - получить элемент по индексу");
    println("3 - очистить");
    println("4 - сумма элементов");
    println("5 - длина списка");
    println("6 - пример выполнения ф-ций высшего порядка");
    println("^C - выход");
}

void safeInput(char *str, int *var) {
    printf(str, "");
    while (scanf("%d", var) == 0) {
        while (getchar() != '\n') {}
        println("Ошибка ввода!");
        printf(str, "");
    }
}


int main(int argc, char **argv) {
    Node *head = NULL;
    int exec = 1;

    printHelp();

    while (exec) {
        int command;
        int number;
        safeInput("\nВведите команду: ", &command);

        switch (command) {
            case 0:
                safeInput("Введите число для добавления: ", &number);
                head = list_add_front(head, number);
                printf("%d успешно добавлено в список!\n", number);
                list_println(head);
                break;
            case 1:
                safeInput("Введите число для добавления: ", &number);
                head = list_add_back(head, number);
                printf("%d успешно добавлено в список!\n", number);
                list_println(head);
                break;
            case 2:
                safeInput("Введите индекс элемента: ", &number);
                if (list_node_at(head, number) != NULL) {
                    printf("list[%d] = %d\n", number, list_node_at(head, number)->value);
                } else println("Индекс выходит за границы размеров массива!");
                break;
            case 3:
                head = list_clear(head);
                list_println(head);
                break;
            case 4:
                printf("Сумма элементов списка: %d\n", list_sum(head));
                break;
            case 5:
                printf("Длина списка: %d\n", list_length(head));
                break;
            case 6:
                test(head);
                break;
            default:
                println("Такой команды нет");
                printHelp();
        }
    }
}
