#include "../headers/tests.h"
#include <stdio.h>

int squares(int value) {
    return value * value;
}

int cubes(int value) {
    return value * value * value;
}

int sum(int x, int a) {
    return x + a;
}

int powerTwo(int x) {
    return x * 2;
}

int min(int x, int a) {
    return x > a ? a : x;
}

int max(int x, int a) {
    return x > a ? x : a;
}

int abs(int x) {
    return x < 0 ? -x : x;
}

void printInt(const int v) {
    printf("%d ", v);
}

void printIntLn(const int v) {
    printf("%d\n", v);
}
