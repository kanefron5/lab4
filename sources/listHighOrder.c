#include "../headers/list.h"
#include <stdio.h>
#include "../headers/listHightOrder.h"

void foreach(Node *head, void (*func)(int)) {
    int len = list_length(head);
    int i;
    for (i = 0; i < len; i++)func(list_node_at(head, i)->value);
}

Node *map(Node *head, int (*func)(int)) {
    int len = list_length(head);
    int i;
    Node *newList = NULL;

    for (i = 0; i < len; i++)
        newList = list_add_back(newList, func(list_node_at(head, i)->value));
    return newList;
}

Node *iterate(int s, int n, int (*func)(int)) {
    int i;
    Node *newList = NULL;

    for (i = 0; i < n; i++) {
        s = func(s);
        newList = list_add_back(newList, s);
    }
    return newList;
}


void map_mut(Node *head, int (*func)(int)) {
    int len = list_length(head);
    int i;

    for (i = 0; i < len; i++)
        list_set_node_at(head, i, func(list_node_at(head, i)->value));
}

int foldl(Node *head, int acc, int (*func)(int, int)) {
    int len = list_length(head);
    int i;

    for (i = 0; i < len; i++)
        acc = func(list_node_at(head, i)->value, acc);
    return acc;
}

bool save(Node *head, const char *filename) {
    int len = list_length(head);
    int i;
    int has_error;

    FILE *file = fopen(filename, "w");
    if (!file) return false;

    for (i = 0; i < len; i++) fprintf(file, "%d ", list_node_at(head, i)->value);

    has_error = ferror(file);
    fclose(file);
    return !has_error;
}

bool load(Node **head, const char *filename) {
    int res;
    int has_error;

    FILE *file = fopen(filename, "r");
    if (!file) return false;
    while (fscanf(file, "%d ", &res) == 1) {
        *head = list_add_back(*head, res);
    }
    has_error = ferror(file);
    fclose(file);
    return !has_error;
}

bool serialize(Node *head, const char *filename) {
    int len = list_length(head);
    int i;
    int has_error;
    FILE *file = fopen(filename, "wb");
    if (!file) return false;
    for (i = 0; i < len; i++)
        fwrite(&list_node_at(head, i)->value, sizeof(list_node_at(head, i)->value), 1, file);

    has_error = ferror(file);
    fclose(file);
    return !has_error;
}

bool deserialize(Node **head, const char *filename) {
    int res;
    int has_error;

    FILE *file = fopen(filename, "rb");
    if (!file) return false;
    while (fread(&res, sizeof(res), 1, (FILE *) file) == 1) {
        *head = list_add_back(*head, res);
    }
    has_error = ferror(file);
    fclose(file);
    return !has_error;
}



