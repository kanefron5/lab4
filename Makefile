all: clean main

main:
	gcc -c sources/main.c sources/list.c sources/listHighOrder.c sources/tests.c -pedantic -Wall -Werror
	gcc -o main main.o list.o listHighOrder.o tests.o -lm

clean:
	rm -f main *.o filename.txt filename.bin
